/**
* @type MTUI2.0
* @author : Mantou
*/
import './utils/isIE';

import Grid from './grid/Grid';
import Button from './button/Button';
import Input from './input/Input';
import PageList from './pagelist/PageList';
import Modal from './modal/Modal';
import Dropdown from './dropDown/Dropdown';
import Select from './select/Select';
import BackTop from './backtop/BackTop';
import Tabs from './tabs/Tabs';
import DatePicker from './datePicker/DatePicker';
import DatePickers from './datePickers/DatePickers';
import Switch from './switch/Switch';
import Radio from './radio/Radio';
import Checkbox from './checkbox/Checkbox';
import Panel from './panel/Panel';
import Slider from './slider/Slider';
import SliderBar from './sliderBar/SliderBar';
import Tree from './tree/Tree';
import Validate from './validate/Validate';
import Swiper from './swiper/Swiper';
import Collapse from './collapse/Collapse';
import Popover from './popover/Popover';
import Popconfirm from './popconfirm/Popconfirm';
import Progress from './progress/Progress';
import Limit from './limit/Limit';

import LoadingBox from './loadingBox/LoadingBox';
import LoadingModal from './loadingModal/LoadingModal';

// 工具
import Tip from './tip/Tip';

// 配置信息
export {
    Grid,
    Button,
    Input,
    Tip,
    PageList,
    Modal,
    Dropdown,
    Select,
    BackTop,
    Tabs,
    DatePicker,
    DatePickers,
    Switch,
    Radio,
    Checkbox,
    Panel,
    Slider,
    SliderBar,
    Tree,
    Validate,
    Swiper,
    Collapse,
    Popover,
    LoadingBox,
    LoadingModal,
    Progress,
    Popconfirm,
    Limit
};